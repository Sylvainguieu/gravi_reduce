#!/data/pionier/anconda/bin/python
from gravi_reduce import local, log, ERROR, WARNING, NOTICE, open_fits

if __name__ == "__main__":
    import time
    import os

    def nfiledir():
        return len([name for name in os.listdir('.') if os.path.isfile(name)])
    dtime = 10
    outputdir = "reduced"

    ##
    # create the reduce directory if needed
    try:
         os.mkdir(outputdir)
    except:
        pass
    else:
        log("'%s' directory created"%outputdir, 1, NOTICE)

    ##
    # get all the *.fits file and dispatch tham in raw dictionary
    raw = open_fits()

    nfile = 0
    i = 0
    first = True
    while True:
        n_nfile = nfiledir()

        if n_nfile==nfile:
            log("wait %d'' for new file (%d)"%(dtime,i), 1, NOTICE)
            time.sleep(dtime)
            i+=1
            continue
        if not first:
            # add more files in raw
            raw.update(local.ls())
        i=0
        nfile = n_nfile
        #####
        # build all the sof
        sofs = raw.build_sof()

        #####
        # reduce all stand alone dark
        # a selection by TPL NAME is Done for that
        sofs.dark.get_tpl_name.select('GRAVITY_gen_cal_dark').run(outputdir)

        ##
        # reduce all the p2vm
        sofs.p2vm.run(outputdir)


        time.sleep(dtime)
        first = False
