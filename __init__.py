"""
Grav reduce is an utility to automaticaly create Set of Files (SOF) and run
the esorex reduction pipeline.

Quite simple to use:

# open all fits file in the curent directory
raw = open_fits("*.fits")

# just return all dark
raw.dark
# or all flat
raw.flat

# build all SOF one can build from the list of file
sofs = raw.build_sof()

# build SOF for FLATs only:
# need to put raw at first argument since darks are in raw
flat_sof = raw.flat.build_sof(raw)

# run all the flat sofs data reduction :
sofs.flat.run()
# or one can do:
raw.flat.build_sof(raw).run()

# run the sof of stand alone dark:
sofs.dark.get_tpl_name.select('GRAVITY_gen_cal_dark').run()
# or
sofs.dark.get_keyword.select('GRAVITY_gen_cal_dark', 'ESO TPL NAME').run()
# or
raw.get_tpl_name.select('GRAVITY_gen_cal_dark').build_sof(raw).run()
#etc ...


"""

####
# debug
#

if False:
    # for debug only
    import getmethod
    reload(getmethod)
    import log
    reload(log)
    import esorex
    reload(esorex)
    import fits
    reload(fits)
    import dark
    reload(dark)
    import flat
    reload(flat)
    import wave
    reload(wave)
    import p2vm
    reload(p2vm)
    import assoc
    reload(assoc)


###
# end debug
#

from fits import pyfits, GravHDUList, GravFitsList, grav_open
from dark import Dark, Darks, DarkSOF, DarkSOFList
from flat import Flat, Flats, FlatSOF, FlatSOFList
from wave import Wave, Waves, WaveSOF, WaveSOFList
from p2vm import P2vm, P2vms, P2vmSOF, P2vmSOFList

from log import Log, ERROR,WARNING, NOTICE
from config import DARK, FLAT, WAVE, P2VM

log = Log().log

from opener import Opener
class Local(Opener):
    """open *.fits file in the local directory by default """
    path = "*.fits"
    opener = grav_open
local = Local()
lsdir = local.ls
open_fits = GravFitsList.fromglob











