from .fits  import (GravHDUList, GravFitsList, GravSOF,
                    GravSOFList, record_class)
from .getmethod import build_getmethod, getarray, getmethod
from .assoc import assoc
from .errors import MissingData, WrongData
import numpy as np
from log import ERROR, WARNING, NOTICE
from .config import FLAT, DARK, P2VM, esorex_options
from .esorex import run


class Flat(GravHDUList):
    def _check_data(self):
        ### check here if it is a flat
        pass

class Flats(GravFitsList):
    """ a list of flats """
    pass

class FlatSOF(GravSOF):
    """ containes a list of SOF dark file """
    ##
    # All the method are called to the first item
    sof_type = FLAT
    esorex_options = esorex_options[FLAT]

    def checkout(self):
        """
        Return the file name of the First Flat found.
        """
        ndark = 0
        nflat = 0
        shutters = 0

        for obj in self:
            otype= obj.get_type()
            if otype == FLAT:
                nflat += 1
                shutters += obj.get_shutters()

            if otype == DARK:
                ndark += 1

        if not ndark:
            raise MissingData("Missing a dark file for FLAT SOF expecting at least 1 got 0")
        if nflat!=4:
            raise MissingData("Missing or to much flat file for FLAT SOF expecting 4 got %d"%nflat)

        if shutters!=(2+4+8+16):
            raise WrondData("The shutter sequence of Flat is not corect")


    def assoc_all_dark(self, darklist):
        return self.assoc_by_type(self.get_type(), DARK,
                                  darklist)

    def assoc_calib(self, alldata):
        darks = self.assoc_all_dark(alldata)

        if not len(darks):
            return FlatSOF(self)


        if len(darks)>1:
            ### we need to choose the right dark
            time_diffs = np.array([self.get_time_diff(dark) for dark in darks])
            closest_index = time_diffs.argmin()
            dark = darks[closest_index]
        else:
            dark = darks[0]

        self.log("Dark Association: Found a dark for SOF '%s', with a time difference of %f hours"%(self.get_product_name(),self.get_time_diff(dark)),
                 1, NOTICE
                )
        ###
        # do not forgot to add self wich is the base sof
        return FlatSOF([dark]+self)


    def _esorex_simulation(self, outputdir="."):
        flat = None
        for obj in self:
            if obj.get_type() == FLAT:
                flat = obj
                break

        new = GravHDUList(flat)
        prod = self.get_product_name()
        self.log("Building reduced file '%s'"%prod, 1, NOTICE)
        new.writeto("%s/%s_0000.fits"%(outputdir,prod), clobber=True)
        return prod



class FlatSOFList(GravSOFList):
    """ containes a list of SOF flat file """
    pass

## mendatory :  need to record all the classes
record_class(FLAT, Flat, Flats, FlatSOF, FlatSOFList)

