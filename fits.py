try:
    import pyfits
except:
    from astropy.io import fits as pyfits
from .log import Log
from .getmethod import getmethod, getarray, build_getmethod
from .opener import Opener
from .assoc import assoc
from log import Log, ERROR, WARNING, NOTICE
from .esorex import run as run_esorex
from errors import MissingData, WrongData

try:
    #old python 2.7
    from StringIO import StringIO
except:
    # new python 3.x
    from io import StringIO

import os
from config import FLAT, DARK, P2VM, WAVE, copy_file
log = Log().log


def get_type(hdu):
    """
      From a Primary HDU Return the file type
      PRO CATG if it is a reduced data
      DPR TYPE if it is a raw data
    """
    prim = hdu
    if "ESO PRO CATG" in prim.header:
        return prim.header["ESO PRO CATG"]
    else:
        return prim.header.get("ESO DPR TYPE", "")

def grav_open(filepath, *args, **kwargs):
    """
    open a fits file for grav_reduce
    because many file can be opened, just doing a pyfits.open would
    result to python bug beacause too many files will opened at the same
    time. So instead grav_open open the file and copy the header in a
    new Primary hdu.
    """
    if not copy_file:
        hdul = pyfits.open(filepath, *args, **kwargs)
        tpe = get_type(hdul[0])
        if tpe in classes_for_type:
            return classes_for_type[tpe][0](hdul)
        return GravHDUList.open(filepath, *args, **kwargs)

    with pyfits.open(filepath, *args, **kwargs) as pf:
        h = pf[0].header.copy()
        finfo = pf[0].fileinfo()
        if finfo is None:
            ofpath = ""
        else:
            ofpath = finfo['file'].name

        #h['OFPATH'] = ofpath # OFPATH is the original file path
        #                     # obsolutly necessary for get_file_path
        pr = pyfits.PrimaryHDU(header=h)
        # file path stored in the primary hdu as a attribute instead of header
        pr._file_path = ofpath

    tpe = get_type(pr)
    if tpe in classes_for_type:
        return classes_for_type[tpe][0]([pr])
    return GravHDUList([pr])


## dictionary containing all classes for each types
## handled by record_class function
classes_for_type = {}
def record_class(type_str, fits_class, fits_list_class,
                 sof_class, sof_list_class):
    """ record all the classes for a specific type of file """
    classes = (fits_class, fits_list_class,
                 sof_class, sof_list_class)
    ##
    # put all classes available for each
    for cl in classes:
        cl.classes = classes

    build_getmethod(fits_list_class, [(True,fits_class)])
    build_getmethod(sof_class, [("master", fits_class)])
    build_getmethod(sof_list_class,[(True, sof_class)])
    classes_for_type[type_str] = classes

    def get_all_type(self):
        return fits_list_class(self.get_type.select(type_str))
    get_all_type.__doc__ = "Return all files of type '%s'"%type_str
    def get_all_sof_type(self):
        return sof_list_class(self.get_type.select(type_str))
    get_all_sof_type.__doc__ = "Return all sof of type '%s'"%type_str

    if not hasattr(GravFitsList, type_str.lower()):
        setattr(GravFitsList, type_str.lower(), property(get_all_type))
    if not hasattr(GravSOFList, type_str.lower()):
        setattr(GravSOFList, type_str.lower(), property(get_all_sof_type))


class NoVALUE(object):
    """dummy class to parse an optional keyword that can be None"""
    pass


class GravHDUList(pyfits.HDUList, Log):
    """ Gravity HDUList that add some more capabilities """
    _extention_keyword = "PRIMARY"

    def __init__(self,*args, **kwargs):
        pyfits.HDUList.__init__(self, *args, **kwargs)
        self._check_data()

    @classmethod
    def fromfile(cl, file_name, *args, **kwargs):
        """
        Open the fits file in a hdu list of its type.
        use obj.get_type() to know the type

        Other Argument are the same than pyfits open function.
        """
        return cl(pyfits.open(file_name, *args, **kwargs))

    def _check_data(self):
        """
        Check if the HDUList has everythong needed for its type.
        e.i. Flat should have a DPR TYPE = Flat keyword etc ...
        So far it is not used. No real need for that
        """
        pass

    @getmethod
    def get_keyword(self, key, default=NoVALUE):
        """
        get the keyword key
        If the keyword does not exists:
            if default is provided, return it
            if not raise a KeyError
        """
        if default is NoVALUE:
            try:
                return self[self._extention_keyword].header[key]
            except KeyError:
                raise KeyError("%s has not keyword '%s'"%(self.get_file_name(),key))
        return self[self._extention_keyword].header.get(key, default)

    @getmethod
    def has_keyword(self, key):
        """ return True if keyword KEY is in the header, False otherwhise """
        return key in self[self._extention_keyword].header


    get_tpl_start = getarray.keyword("ESO TPL START")
    get_date_obs  = getarray.keyword("DATE-OBS")
    get_mjd_obs   = getarray.keyword("MJD-OBS")
    get_dpr_type  = getarray.keyword("ESO DPR TYPE")
    get_tpl_name  = getarray.keyword("ESO TPL NAME")

    @getmethod
    def get_shutters(self):
        """Return a binary number stating the state of all for shutter
        return shut1_state*2 + shut2_state*4 + shut3_state*8 + shut4_state*16
        """
        return self.get_keyword("ESO INS SHUT11 ST")*2+\
               self.get_keyword("ESO INS SHUT12 ST")*4+\
               self.get_keyword("ESO INS SHUT13 ST")*8+\
               self.get_keyword("ESO INS SHUT14 ST")*16

    @getmethod
    def get_type(self):
        """ Return the file type
          PRO CATG if it is a reduced data
          DPR TYPE if it is a raw data
        """
        if self.has_keyword("ESO PRO CATG"):
            return self.get_keyword("ESO PRO CATG")
        else:
            return self.get_keyword("ESO DPR TYPE", "")

    @getmethod
    def get_do_catg(self):
        """
        return the do catg writen in sof file (e.g. DARK_RAW)
        """
        if self.has_keyword("ESO PRO CATG"):
            return self.get_keyword("ESO PRO CATG")
        if self.has_keyword("ESO DPR TYPE"):
            return self.get_keyword("ESO DPR TYPE")+"_RAW"

        raise WrongData("Cannot determine the do_catg both 'ESO PRO CATG' and 'ESO DPR TYPE' are missing")


    @getmethod
    def get_file_path(self):
        """return the fits file path
        see also
        --------
        get_file_name
        """
        if hasattr(self[0], "_file_path"):
            return self[0]._file_path

        if self.has_keyword("OFPATH"):
            return self.get_keyword("OFPATH")
        finfo = self["PRIMARY"].fileinfo()
        if finfo is None: return ""
        return finfo['file'].name

    @getmethod
    def get_file_name(self):
        """ return the fits file name
        see also
        --------
        get_file_path
        """
        return os.path.split(self.get_file_path())[1]


    @getmethod
    def get_reduced_prefix(self):
        """ return the reduced file name prefix of this file """
        fname = self.get_file_name()
        sname = os.path.splitext(fname)
        return "r."+sname[0]

    @getmethod
    def has_reduced_file(self, outputdir="./reduced"):
        """ return true if found a reduced file in outputdir
        the reduce file name is the reduced prefix ending by _0000.fits
        e.g.:
          GRAVI_DARK.fits check if r.GRAVI_DARK_0000.fits is in outputdir
        """
        return os.path.exists("%s/%s_0000.fits"%(outputdir, self.get_reduced_prefix()))


    @getmethod
    def is_reduced(self):
        """ return True if the file is a reduce product """
        return self.has_keyword("ESO PRO CATG")

    @getmethod
    def is_raw(self):
        """ return True is the file is a raw data """
        return not self.is_reduced()


    @getmethod
    def append_sof(self, fh):
        """ write a sof line on the opened file (only argument) """
        return fh.write("%s\t%s\n"%(self.get_file_path(),
                                   self.get_do_catg()
                        )
                       )

    @getmethod
    def get_time_diff(self, obj):
        """ return the time difference *in hour* between self and
        an other fits file
        """
        time_dif = self.get_mjd_obs()-getattr(obj,"get_mjd_obs",
                                              lambda o:o.header["MJD-OBS"])()
        return time_dif/24.

    @getmethod
    def assoc_by_type(self,tpfinder, tptarget, lst):
        """
        obj.assoc_by_type(tpfinder, tptarget, lst)

        take 3 arguments:
          tpfinder : the type of the file to look for ...
          tptarget : the type of the file to find
          lst : a list of target object

        The association is done thanks to the assoc directory defined
        in the assoc.py file.

        """

        config_func = assoc[tpfinder][tptarget]
        self_config = config_func(self)
        output = []
        for obj in lst:
            if obj.get_type()!= tptarget:
                continue
            if config_func(obj)==self_config:
                output.append(obj)
        return output


    def build_self_sof(self, lst):
        """
        build a sof containing only the object of its type

        see also methods
        ---------------
        build_sof, assoc_calib
        """
        ftype = self.get_type()
        sof_class = self.classes[2]
        return sof_class( self.assoc_by_type(ftype, ftype,lst) )

    def build_sof(self, lst):
        """
        Build and return the sof object containing all file necessary
        in the input argument which can be a flat list of a dictionary
        where list are sorted by time

        The association is done thanks to the assoc directory defined
        in the assoc.py file.

        see also methods
        ---------------
        build_self_sof, assoc_calib
        """
        return self.build_self_sof(lst).assoc_calib(lst)


    def __repr__(self):
        return "%s(%s)"%(self.get_type(),self.get_file_name())


class FitsOpenList(Opener):
    path   = "*.fits"
    opener = staticmethod(grav_open)

class GravFitsList(list, Log):
    opener = FitsOpenList()

    def _check_data(self):
        """
        check the validity of the list
        do nothing if it does not matter
        """
        pass

    @classmethod
    def fromglob(cl, path=None, subdir=None, rootdir=None):
        """
        open several file with a glob path
        by default look '*.fits' in the curent directory.
        Result is return inside a flat list of Fits files
        """
        lst = cl.opener.ls(path=path, subdir=subdir,
                           rootdir=rootdir
                          )
        return cl.fromlist(lst)

    @classmethod
    def fromlist(cl, lst):
        """ open fits file of a list of path """
        output = []
        for fl in lst:
            hdul = cl.opener.opener(fl)
            tpe = hdul.get_type()
            if not tpe in classes_for_type:
                log("'%s' as unknown type '%s' and will be ignored"%(fl,tpe), 1, WARNING)
            else:
                log("'%s' is a '%s"%(fl,tpe), 1, NOTICE)
                output.append(hdul)
        return cl(output)

    def update(self, lst):
        self_path = self.get_file_path()
        for hdul in lst:
            already_opened = False
            if isinstance(hdul, basestring):
                fname = hdul
            else:
                fname = hdul.get_file_path()

            for path in self_path:
                if os.path.samefile(path, hdul):
                    already_opened = True
                    break
            if  already_opened:
                continue

            if isinstance(hdul, basestring):
                hdul =  self.opener.opener(hdul)

            tpe = hdul.get_type()
            if not tpe in classes_for_type:
                log("'%s' as unknown type '%s' and will be ignored"%(hdul.get_file_name(),tpe), 1, WARNING)
            else:
                log("'%s' is a '%s"%(hdul.get_file_name(),tpe), 1, NOTICE)

                self.append(hdul)





    def build_self_sof(self, lst=None, matched=None):
        """
            make a list of set of file with the file of same type
            matched eachother if needed.
        """
        soflist_class = self.classes[3]
        lst = lst or self

        output = soflist_class([])

        if matched is None:
            matched = {}

        for i,flat in enumerate(self):
            if flat.get_file_name() in matched:
                continue
            sof = flat.build_self_sof(lst)
            fnames = []
            for m in sof:
                fname = m.get_file_name()
                fnames.append(fname)
                matched[fname] = sof

            self.log("Creation of SOF with %d files:\n\t\t%s"%(len(sof),"\n\t\t".join(fnames)),
                     1, NOTICE
                     )
            output.append(sof)
        return output

    def build_sof(self, lst=None):
        """
        Build a list of SOF.
        the first argument (optional) is a list of file, by default this is the
        list itself.

        """
        lst = lst or self
        return self.build_self_sof(lst).assoc_calib(lst)

    def __repr__(self):
        return "%s(%s)"%(self.__class__.__name__,
                         list.__repr__(self)
                         )
#build_getmethod(GravFitsList, [(True,GravHDUList)])

class GravSOF(list, Log):
    """ Contain a list of file that represent a set of file of its type """
    products = []
    sof_type = ""

    def __getitem__(self,item):
        if isinstance(item, basestring):
            if item == "master":
                return self.get_master()
            raise KeyError("Item '%s' not understood, should be int or 'master'"%item)
        return list.__getitem__(self,item)

    def  __str__(self):
        fh = StringIO()
        for obj in self:
            obj.append_sof(fh)
        return fh.getvalue()

    def __repr__(self):
        return self.__str__()


    def checkout(self):
        """ check if the SOF is complete
          if not should raise a MissingData or WrongData error
        """
        ## key function check if the SOF is ready to be
        ## executed. Must be changed by each types
        pass


    def get_master(self):
        """
        Return The master fits file of the SOF
        by default this is the first in time of its kind.
        e.g. for a SOF of FLAT, return the first FLAT found

        Most of other function, get_keyword, get_file_name, etc ...
        are called to the master file.
        """
        if not len(self): #should not happen normaly
            return None

        mjd = [f.get_mjd_obs() for f in self]
        ## a sorted index:
        sorted_index = sorted(range(len(mjd)), key=mjd.__getitem__)
        # return the first object of its type found
        selftype = self.get_type()
        for i in sorted_index:
            if self[i].get_type() == selftype:
                return self[i]
        # otherwhise return the first in time of the list
        return self[sorted_index[0]]

    @getmethod
    def get_type(self):
        ## the type of the SOF msut be writen in the class
        return self.sof_type


    @getmethod
    def get_product_name(self):
        """ Return the file produc name of this SOF """
        return os.path.splitext(self.get_master().get_file_name())[0]

    def assoc_calib(self, alldata):
        """ For a SOF the assoc_calib is called from the master
            object and concatenated with itself
        """
        return self.__class__(self.get_master().assoc_calib(alldata)+self)


    @getmethod
    def get_reduced_prefix(self, outputdir="./reduced"):
        return "%s/%s_0000.fits"%(outputdir, self.get_product_name())


    @getmethod
    def has_reduced_file(self, outputdir="./reduced"):
        """ return true if found a reduced file in outputdir """
        return os.path.exists(self.get_reduced_prefix(outputdir))

    def run(self, outputdir="./reduced"):
        return run_esorex(self, outputdir=outputdir)
#build_getmethod(GravSOF, [("master", GravHDUList)])

class GravSOFList(list, Log):
    """ A list of set of file """

    def __str__(self):
        text = ""
        for obj in self:
            text += "\n\n#========= SOF %s ========\n"%(obj.get_product_name())
            text += str(obj)
        return text

    def __repr__(self):
        return self.__str__()

    def assoc_calib(self, alldata):
        return self.__class__([sof.assoc_calib(alldata) for sof in self])

    def run(self, outputdir="./reduced"):
        for sof in self:
            try:
                sof.run(outputdir=outputdir)
            except (MissingData, WrongData) as e:
                log("Can not execute SOF '%s' : %s"%\
                      (sof.get_product_name(), e.message), 1, ERROR)

#build_getmethod(GravSOFList, [(True,GravSOF)])

record_class("", GravHDUList, GravFitsList, GravSOF, GravSOFList)
#classes = (GravHDUList, GravFitsList, GravSOF, GravSOFList)
#for cl in classes:
#    cl.classes = classes

