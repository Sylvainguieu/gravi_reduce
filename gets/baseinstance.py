
def on2iterable(obj,on):
    """ convert any on True/list/slice to a list of key
    function to the object nature
    """
    if on is True:
        if hasattr(obj, "iterkeys"):
            return obj.iterkeys()
        return range(len(obj))

    if isinstance(on, slice):
        on = range(*on.indices(len(obj)))
        if hasattr(obj, "keys"):
            keys = obj.keys()
            return [keys[i] for i in on]
        return on
    return on


class BaseInstance(object):
    _attrs_ = []
    def __init__(self, obj):
        self.obj = obj
    def __call__(self, *args, **kwargs):
        return self.get(*args, **kwargs)
    def new(self,fget):
        new = self.__class__(self.obj)
        for k in self._attrs_:
            if hasattr(self, k):
                setattr(new, k, getattr(self,k))
        new.fget = fget
        return new

try:
    import numpy as np
    class BaseNumerical(object):
        def min(self, *args, **kwargs):
            return np.min( self.get(*args, **kwargs))
        def max(self, *args, **kwargs):
            return np.max( self.get(*args, **kwargs))
        def mean(self, *args, **kwargs):
            return np.mean( self.get(*args, **kwargs))
        def median(self, *args, **kwargs):
            return np.median( self.get(*args, **kwargs))
        def std(self, *args, **kwargs):
            return np.std(self.get(*args, **kwargs))
except:
    class BaseNumerical(object):
        pass
