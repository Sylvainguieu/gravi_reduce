"""
Wrapper object are used to wrap and iterate throught a list or dict of object
and build a a new object

they must have:
* init method with signature (obj, on, args, kwargs)
        obj is the iterable obj.
        on is True (for all the object) or a list that specify only a part of
            object index.
        args  the args tuple of the function call
        kwargs  the kwargs dict of the function call, they can alterate
        the iteration, init can also modify them, e.g. to remove keyword
        specific of building the iteration
        init must return an iterable object (usualy self)
        This iterator must return key,child at each next.
        key is the curent key and chil the curent obj child.

* add  with signature (key, value)
       add the returned value inside the builded object.
       the key can have been changed from what return by iterator
* end  clean up the builded/wrapped object and return it

* __call__ with no argument it should return a new wrapper instance
see examples bellow

if init return self, it should have
* __iter__  to return the iterator (usualy self)
            the iterator must return 4 items at each iteration:
               - the key
               - the object to wich to apply fget
               - args list for fget
               - kwarg dict for fget
If __iter__ return self, next must be present and should return what describde
above.
"""

from .baseinstance import on2iterable
from . import config
use_pandas = getattr(config, "use_pandas", False)
use_numpy  = getattr(config, "use_numpy", True)

if use_pandas:
    try:
        import pandas
    except:
        use_pandas = False

if use_numpy:
    try:
        import numpy as np
    except:
        use_numpy = False



def iswrapperinstance(wrapper):
    return isinstance(wrapper, BaseWrapper) or \
                (type(wrapper) is type and\
                 issubclass(wrapper, BaseWrapper))

class BaseWrapper(object):
    _attrs_ = []

    def get_iter(self, obj, on):
        return on2iterable(obj, on)

    def get_obj(self,obj):
        return obj

    def get_output(self):
        return []


    def init(self, obj, on, args, kwargs):
        self.args   = list(args)
        self.kwargs = kwargs

        self.on  = self.get_iter(obj, on)
        self.obj = self.get_obj(obj)

        self.output = self.get_output()
        self.size = len(self.on)

        return self

    def __iter__(self):
        self.index = -1
        return self

    def next(self):
        self.index += 1
        if self.index>=self.size:
            raise StopIteration()
        self.key =  self.on[self.index]
        return self.key, self.obj[self.key], self.args, self.kwargs

    def add(self, key, value):
        self.output.append(value)

    def end(self):
        output = self.output
        del self.output
        del self.obj
        return output

    def __call__(self, *args, **kwargs):
        Nargs = len(args)
        for i, value in enumerate(args):
            attr = self._attrs_[i]
            if attr in kwargs:
                raise TypeError("getmethod() got multiple values for keyword argument '%s'"%attr)
            kwargs[attr] = value


        for k in self._attrs_:
            if kwargs.get(k,None) is None:
                kwargs[k] = getattr(self,k)

        # clean up the None values
        kwargs = { k:v for k,v in kwargs.iteritems() if v is not None }
        return self.__class__(**kwargs)


class ListWrapper(BaseWrapper):
    pass

class DictWrapper(BaseWrapper):
    def get_output(self):
        return {}

    def add(self, key, value):
        self.output[key] = value

    @staticmethod
    def values(d):
        return d.values()

class get_iter_mod(object):
    def __init__(self,obj):
        self.obj = obj
        self.iter = obj.__iter__()
    def next(self):
        try:
            n = self.iter.next()
        except:
            self.iter = self.obj.__iter__()
            n = self.iter.next()
        return n

class get_obj(object):
    def __init__(self,obj):
        self.obj = obj
    def next(self):
        return self.obj

def get_iter(obj):
    return obj.__iter__()



class ArgsWrapper(BaseWrapper):
    wrapper  = list
    loopargs = None
    lenargs = None
    _iswrapper = False
    items = None
    mode = "mod"
    @staticmethod
    def flen(value):
        return len(value) if isinstance(value, list) else 0

    fgetiter = None
    _attrs_ =["loopargs", "lenargs", "wrapper", "items",
               "flen", "mode"
               ]
    def __init__(self, loopargs=None, lenargs=None, wrapper=None,
                 items=None, flen=None, mode=None):
        if loopargs is not None:
            self.loopargs = loopargs
        if lenargs is not None:
            self.lenargs = lenargs
        if wrapper is not None:
            if iswrapperinstance(wrapper):
                wrapper = wrapper()
                self._iswrapper = True
            self.wrapper = wrapper

        if items is not None:
            self.items = items

        if flen is not None:
            self.flen = flen
        if mode is not None:
            self.mode = mode


    def init_loopargs(self, args, kwargs):
        loopargs = self.loopargs or range(len(args))+kwargs.keys()
        lenargs  = self.lenargs or loopargs
        flen = self.flen
        lens = {k:0 for k in range(len(args))+kwargs.keys()}

        minlen = 99999
        maxlen = 0
        difflen = 0
        cumlen = 0
        allargs = dict(list(enumerate(args))+kwargs.items())


        if self.fgetiter is None:
            fgetiter = get_iter_mod if self.mode == "mod" else get_iter
        else:
            fgetiter = self.fgetiter

        iterators = {k:get_obj(v) for k,v in allargs.iteritems()}

        for k in loopargs:
            if not k in allargs:
                continue
            v = allargs[k]

            n = flen(v)
            cumlen += n

            if k in lenargs:
                minlen = min(minlen, n)
                maxlen = max(maxlen, n)
                difflen = max(n-minlen , maxlen-n)

            if n:
                iterators[k] = fgetiter(v)
            lens[k] = n

        if self.mode == "min":
            self.size = minlen
        elif self.mode == "mod":
            self.size = maxlen
        elif difflen>0:
            raise ValueError("All lenargs should have the same size when modulus=False")

        self.scalar = False
        if cumlen and not self.size:
            # size is 0 but they are some loopable
            # object left need to iter on first arg
            self.scalar = True
            self.size = 1


        self.lens = lens
        self.iterators = iterators

        self.allargs = allargs
        self.nargs = len(args)
        self.keys = kwargs.keys()
        self.output = []

    def init(self, obj, on, args, kwargs):
        self.init_loopargs(args, kwargs)
        self.obj = obj
        if not self.size:
            return None
        if self._iswrapper:
            # trick the wrapper and send a false on list
            self.wrapper.init(obj, list(range(self.size)), [],{})
            self.wrapiter = self.wrapper.__iter__()
        return self

    def add(self, key, value):
        if self.scalar:
            self.output = value
            return

        if self._iswrapper:
            self.wrapper.index += 1
            self.wrapper.add(key, value)
        else:
            items  = self.items if self.items is not None else hasattr(self.wrapper, "iteritems")
            if items:
                self.output.append( (key,value) )
            else:
                self.output.append(value)

    def next(self):
        self.index += 1
        if self.index>=self.size:
            raise StopIteration()
        self.key =  self.index
        allargs = self.allargs

        iterators = self.iterators
        args   = [iterators[i].next() for i in range(self.nargs)]
        kwargs = {k:iterators[k].next()  for k in self.keys }
        return self.key, self.obj, args, kwargs

        #lens = self.lens
        #args   = [allargs[i][self.index%lens[i]] if lens[i] else allargs[i] for i in range(self.nargs)]
        #kwargs = {k:allargs[k][self.index%lens[k]] if lens[k] else allargs[k] for k in self.keys }
        #return self.key, self.obj, args, kwargs

    def end(self):
        if self.scalar:
            return self.output

        if self._iswrapper:
            return self.wrapper.end()
        output = self.output
        obj = self.obj
        del self.output
        del self.obj
        return self.wrapper(output)





