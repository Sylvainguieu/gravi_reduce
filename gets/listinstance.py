from .baseinstance import BaseInstance, BaseNumerical, on2iterable
from . import config
use_pandas = getattr(config, "use_pandas", False)
use_numpy  = getattr(config, "use_numpy", True)

if use_pandas:
    try:
        import pandas
    except:
        use_pandas = False

if use_numpy:
    try:
        import numpy as np
    except:
        use_numpy = False

def get_iterable(obj):
    if hasattr(obj, "iteritems"):
        return obj.iteritems()
    return enumerate(obj)

def guess_wrapper(obj):
    if hasattr(obj, "iteritems"):
        return dict, True
    if isinstance(obj, list):
        return list, False

    if use_numpy and isinstance(obj, np.ndarray):
        return np.array, False
    return list, False

def values2set(values):
    """ transform a list to a sorted list with unic values """
    values = list(set(values))
    values.sort()
    return values

class ListInstance(BaseInstance):
    selfwrapper = None
    wrapper = list
    _attrs_ = ["wrapper", "selfwrapper"]

    def iter(self, *args, **kwargs):
        ikwargs = kwargs.pop("ikwargs", {})
        allvalues = self.get(*args,**kwargs)
        itervalues = values2set(allvalues)
        def subfunc(value):
            test = [value==allval for allval in allvalues]
            return self.sub(test, *args, **kwargs)

        name = getattr(self, "name", "value") or "value"

        return IterInstance(subfunc, itervalues,
                            _name=name,
                            **ikwargs
                           )

    def map(self, mapfunc, *args, **kwargs):
        def fget(obj, *args, **kwargs):
            return mapfunc( self.fget(obj, *args, **kwargs))
        kwargs["__fget__"] = fget
        return self.get(*args, **kwargs)

    def mapvalues(self, mapfunc, *args, **kwargs):
        """ same as map but result is forced to be a list """
        def fget(obj, *args, **kwargs):
            return mapfunc( self.fget(obj, *args, **kwargs))
        kwargs["__fget__"] = fget
        return self.values(*args, **kwargs)

    def mapitems(self, mapfunc, *args, **kwargs):
        """ same as map but result is forced to be a item list"""
        def fget(obj, *args, **kwargs):
            return mapfunc( self.fget(obj, *args, **kwargs))
        kwargs["__fget__"] = fget
        return self.items(*args, **kwargs)

    def filter(self, ffunc, *args, **kwargs):
        return self.sub(self.map(ffunc, *args, **kwargs), *args, **kwargs)

    def select(self, choose_value, *args, **kwargs):
        """ return """
        def ffunc(value):
            return choose_value==value

        return self.filter(ffunc, *args, **kwargs)

    def set(self, *args, **kwargs):
        return values2set(self.values(*args, **kwargs))

    def values(self, *args, **kwargs):
        return list(self.itervalues(*args, **kwargs))

    def items(self, *args, **kwargs):
        return list(self.iteritems(*args, **kwargs))

    def keys(self, *args, **kwargs):
        return list(self.iterkeys(*args, **kwargs))

class _on_true(object):
    """ on is true loop over the entire object """
    def itervalues(self, *args, **kwargs):
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        it = get_iterable(obj)
        return (fget(o, *args, **kwargs) for k,o in it)

    def iteritems(self, *args, **kwargs):
        obj = self.obj
        fget = kwargs.pop( "__fget__", self.fget)
        fkey= kwargs.pop( "__fkey__", self._fkey)
        it = get_iterable(obj)
        return ((fkey(o,k),fget(o, *args, **kwargs)) for k,o in it)

    def iterkeys(self, *args, **kwargs):
        obj = self.obj
        fkey= kwargs.pop( "__fkey__", self._fkey)
        it = get_iterable(obj)
        return (fkey(obj[k],k) for k,o in it)



class _on_lst(object):
    """ on is a list loop over the list """

    def itervalues(self, *args, **kwargs):
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        on = on2iterable(obj, self.on)
        return (fget(obj[i], *args, **kwargs) for i in on)
    def iteritems(self, *args, **kwargs):
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        fkey= kwargs.pop( "__fkey__", self._fkey)
        on = on2iterable(obj, self.on)
        return ((fkey(obj[k],k),fget(obj[k], *args, **kwargs)) for k in on)
    def iterkeys(self, *args, **kwargs):
        obj = self.obj
        fkey= kwargs.pop( "__fkey__", self._fkey)
        on = on2iterable(obj, self.on)
        return (fkey(obj[k],k) for k in on)





class _wrp_lst(object):
    """If the wrapper wants a list """
    def get(self, *args, **kwargs):
        wrapper  =  self.wrapper
        return wrapper(self.values(*args, **kwargs))

class _wrp_items(object):
    """ The wrapper wants a item list  """
    def get(self, *args, **kwargs):
        """ get func when on is True """
        wrapper  =  self.wrapper
        return wrapper(self.items(*args, **kwargs))


class _wrp_any(object):
    """ do not have wrapper, guess it with obj """
    def get(self, *args, **kwargs):
        """ get func when on is True """
        wrapper, items  =  guess_wrapper(self.obj)
        if items:
            return wrapper(self.items(*args, **kwargs))
        else:
            return wrapper(self.values(*args, **kwargs))


class _wrapped(object):
    """
        The wrapper is a BaseWrapper instance, it will handles
        the iteration and building the returned object
    """
    def get(self, *args, **kwargs):
        """ get func when wrapper is a BaseWrapper instance """
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        fkey= kwargs.pop( "__fkey__", self._fkey)

        wrapper  = self.wrapper
        iterable = wrapper.init(obj, self.on,  args, kwargs)
        if iterable is None:
            return fget(obj, *args, **kwargs)

        for k,o, args, kwargs in iterable:
            wrapper.add(fkey(o,k), fget(o, *args, **kwargs))
        return wrapper.end()

    def values(self, *args, **kwargs):
        """ force the get to return a list """
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        wrapper  = self.wrapper
        iterable = wrapper.init(obj, self.on,  args, kwargs)
        if iterable is None:
            return fget(obj, *args, **kwargs)
        return [fget(o, *args, **kwargs) for k,o,_,_ in iterable]
    def items(self, *args, **kwargs):
        """ force the get to return a item list """
        obj = self.obj
        fget= kwargs.pop( "__fget__", self.fget)
        fkey= kwargs.pop( "__fkey__", self._fkey)
        wrapper  = self.wrapper
        iterable = wrapper.init(obj, self.on,  args, kwargs)
        if iterable is None:
            return fget(obj, *args, **kwargs)
        return [(fkey(o,k),fget(o, *args, **kwargs)) for k,o,_,_ in iterable]


class _true_lst(_on_true, _wrp_lst):
    pass

class _true_items(_on_true, _wrp_items):
    pass

class _true_any(_on_true, _wrp_any):
    pass

class _lst_lst(_on_lst,_wrp_lst):
    pass

class _lst_items(_on_lst, _wrp_items):
    pass

class _lst_any(_on_lst, _wrp_any):
    pass




class _sub_on_lst(object):
    def sub_values(self, test):
        obj = self.obj
        return [obj[k] for k in self.on if test[k]]

    def sub_items(self, test):
        obj = self.obj
        return [(k,obj[k]) for k in self.on if test[k]]

class _sub_on_true(object):
    def sub_values(self, test):
        obj = self.obj
        it = get_iterable(obj)
        return [o for k, o in it if test[k]]

    def sub_items(self, test):
        obj = self.obj
        it = get_iterable(obj)
        return [(k,obj[k]) for k,o in it if test[k]]

class _sub_on_wrapped(object):
    """
    sub func if the selfwrapper accept a item list and
    wrapper is BaseWrapper instance
    *args **kwargs are parsed to the wrapper.init() as it may
    modify the iteration
    """
    def sub_items(self, test, *args, **kwargs):

        obj = self.obj

        wrapper = self.wrapper
        iterable = wrapper.init(obj, self.on, args, kwargs)
        if iterable is None:
            return fget(obj, *args, **kwargs)
        return [(k,o) for i,(k,o,_,_) in enumerate(iterable) if test[i]]

    def sub_values(self, test, *args, **kwargs):

        obj = self.obj

        wrapper = self.wrapper
        iterable = wrapper.init(obj, self.on, args, kwargs)
        if iterable is None:
            return fget(obj, *args, **kwargs)
        return [o for i,(k,o,_,_) in enumerate(iterable) if test[i]]




class _sub_get_lst(object):
    """ sub func if the selfwrapper accept a list
        and on is a list
        *args **kwargs has no effect here but exists for
        compatibility with wrapper
    """
    def sub(self, test, *args, **kwargs):
        selfwrapper = self.selfwrapper or  self.obj.__class__
        return selfwrapper(self.sub_values(test))

class _sub_get_any(object):
    """ sub func if the selfwrapper is unknown
        and on is a list
        *args **kwargs has no effect here but exists for
        compatibility with wrapper
    """
    def sub(self, test, *args, **kwargs):
        obj = self.obj
        selfwrapper = self.selfwrapper or  self.obj.__class__
        if hasattr(selfwrapper, "iteritems"):
            return selfwrapper(self.sub_items(test))
        return selfwrapper(self.sub_values(test))

class _sub_get_items(object):
    """ sub func if the selfwrapper accept a item list
        and on is a list
        *args **kwargs has no effect here but exists for
        compatibility with wrapper
    """
    def sub(self, test, *args, **kwargs):
        selfwrapper = self.selfwrapper or  self.obj.__class__
        return selfwrapper(self.sub_items(test))

class _sub_true_lst(_sub_on_true, _sub_get_lst):
    pass
class _sub_true_items(_sub_on_true, _sub_get_items):
    pass
class _sub_true_any(_sub_on_true, _sub_get_any):
    pass
class _sub_lst_lst(_sub_on_lst, _sub_get_lst):
    pass
class _sub_lst_items(_sub_on_lst, _sub_get_items):
    pass
class _sub_lst_any(_sub_on_lst, _sub_get_any):
    pass
class _sub_wrapped_lst(_sub_on_wrapped, _sub_get_lst):
    pass
class _sub_wrapped_items(_sub_on_wrapped, _sub_get_items):
    pass
class _sub_wrapped_any(_sub_on_wrapped, _sub_get_any):
    pass


class _iterable(object):
    """ add iterable capability """
    def __iter__(self):
        self._index = -1
        self._oniter = on2iterable(self.obj, self.on)
        self._size = len(self._oniter)
        return self

    def next(self):
        self._index +=1
        if self._index>=self._size:
            raise StopIteration()
            k = self._oniter[self._index]
        return self.fget(self.obj[k])


listclasses = {"get":{
            "lst":{"lst":_lst_lst,
                   "items":_lst_items,
                   "any":_lst_any
                    },
            "true":{
                 "lst":_true_lst,
                 "items":_true_items,
                 "any": _true_any
                    },
             "wrapped":{
                 "lst":_wrapped,
                 "items":_wrapped,
                 "any":_wrapped
                     }
                },
            "sub":{
                 "true":{
                     "lst":_sub_true_lst,
                     "items":_sub_true_items,
                     "any":_sub_true_any
                      },
                 "lst":{
                     "lst":_sub_lst_lst,
                     "items":_sub_lst_items,
                     "any":_sub_lst_any
                     },
                  "wrapped":{
                    "lst":_sub_wrapped_lst,
                    "items":_sub_wrapped_items,
                    "any":_sub_wrapped_any
                      }
                },
             "iterable":_iterable
             }

if use_numpy:

    def fmap_dstats(value):
        return dict([("size",np.size(value)),
            ("shape",np.shape(value)),
            ("mean",np.mean(value)),
            ("median",np.median(value)),
            ("std",np.std(value)),
            ("max",np.max(value)),
            ("min",np.min(value))
            ])


    class ListNumerical(BaseNumerical):
        def means(self, *args, **kwargs):
            return self.map( np.mean, *args, **kwargs)

        def medians(self, *args, **kwargs):
            return self.map( np.median, *args, **kwargs)

        def maxes(self, *args, **kwargs):
            return self.map( np.max, *args, **kwargs)

        def minis(self, *args, **kwargs):
            return self.map( np.min, *args, **kwargs)

        def stds(self, *args, **kwargs):
            return self.map( np.std, *args, **kwargs)

        def stats(self, *args, **kwargs):
            """ return the instance value in a np.recarray """
            A = None
            def fmap(value):
                return (np.size(value),
                        (np.shape(value)),
                        np.mean(value),
                        np.median(value),
                        np.std(value),
                        np.max(value),
                        np.min(value))


            lst = self.mapvalues(fmap , *args, **kwargs)

            if not len(lst):
                return np.array([])
            first = lst[0]

            dtype = [(k, np.dtype(type(first[i]))) for i,k in \
                enumerate(["size", "shape", "mean", "median", "std", "max", "min"])]

            return np.array( lst, dtype)

        def dstats(self, *args, **kwargs):
            """ return the instance value, each item is a dictionary """
            return dict(self.mapitems(fmap_dstats , *args, **kwargs))

        if use_pandas:
            def pstats(self, *args, **kwargs):

                def fmap_pstats(value):
                    return[np.size(value),
                           np.shape(value),
                           np.mean(value),
                           np.median(value),
                           np.std(value),
                           np.max(value),
                           np.min(value)
                           ]
                data = self.mapitems(fmap_pstats, *args, **kwargs)
                return pandas.DataFrame.from_items(
                          data,
                          columns=["size", "shape", "mean", "median", "std", "max", "min"],
                          orient="index"
                        )

        def choose(self, values, *args, **kwargs):

            obj = self.obj
            wrapper = self.selfwrapper or obj.__class__
            fget = self.fget

            if not isinstance(values, np.ndarray) and hasattr( values, "__call__"):
                values = values(self.get(*args, **kwargs))

            if isinstance( values, np.ndarray):
                def ffunc(value):
                    return any(value == values)

            elif isinstance(values, tuple):
                minval, maxval = values
                if minval is None and maxval is None:
                    def ffunc(value):
                        return True
                elif maxval is None:
                    def ffunc(value):
                        return value>=minval
                elif minval is None:
                   def ffunc(value):
                        return value<=maxval
                else:
                    def ffunc(value):
                        return (value>=minval) & (value<=maxval)

            elif hasattr( values, "__iter__"):
                def ffunc(value):
                    return value in values
            else:
                def ffunc(value):
                    return values==value

            return self.filter(ffunc, *args, **kwargs)
else:
    class ListNumerical(BaseNumerical):
        pass
############################################################
#
#  Smart iterator
#
############################################################

color_cycle  = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
marker_cycle = [',', '+', '-', '.', 'o', '*']



class IterInstance(object):
    _index = -1
    color_cycle  = color_cycle
    marker_cycle = marker_cycle

    def __init__(self, _subfunc, _itervalues,
                 _name="value",
                 **kwargs):
        self._subfunc = _subfunc
        self._itervalues = _itervalues
        self._size = len(self._itervalues)
        self._name = _name

        self.kwargs = kwargs

    def __getitem__(self, item):
        return self._subfunc(self._itervalues[item])

    def next(self):
        self._index += 1
        if self._index>=self._size:
            raise StopIteration()
        return self[self._index]

    def __iter__(self):
        self._index = -1
        return self

    def __len__(self):
        return self._size


    def __getattr__(self, attr):
        if attr in self.kwargs:
            return self.get_kw(attr)
        raise AttributeError("this Iterator does not have attribute '%s' "%attr)

    @property
    def value(self):
        if self._index<0: return self._itervalues[0]
        return self._itervalues[self._index]

    @property
    def legend(self):
        if "legend" in self.kwargs: return self.get_kw("legend")

        return self.get_legend()
    _legend = "{0._name} = {0.value:}"
    def get_legend(self):
        return self._legend.format(self)

    @property
    def color(self):
        if "color" in self.kwargs: return self.get_kw("color")
        return self.color_cycle[self._index%len(self.color_cycle)]
    @property
    def marker(self):
        if "marker" in self.kwargs: return self.get_kw("marker")
        return self.marker_cycle[self._index%len(self.marker_cycle)]
    @property
    def axes(self):
        if "axes" in self.kwargs: return self.get_kw("axes")
        return (self._size, self._index+1)

    @property
    def figure(self):
        if "figure" in self.kwargs: return self.get_kw("figure")
        return self._index+1

    def get_kw(self, key):
        v = self.kwargs[key]
        if hasattr(v, "__iter__") and not isinstance(v,tuple):
            return v[self._index%len(v)]
        return v
    @property
    def kw(self):
        return {k:self.get_kw(k) for k in self.kwargs}




