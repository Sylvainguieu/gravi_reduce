
from errors import MissingData, WrongData
from .log import Log, ERROR, WARNING, NOTICE
import os
from .config import esorex_options

log = Log().log

def write_sof(sof, file_path):
    fh = open(file_path, "w")
    for fits in sof:
        fits.append_sof(fh)
    fh.close()


def run(sof, outputdir="./reduced"):
    try:
        sof.checkout()
    except (MissingData, WrongData) as e:
        log("Can not run SOF '%s' : %s"%\
                      (sof.get_product_name(),e.message), 1, ERROR)

        return



    product = sof.get_product_name()
    if sof.has_reduced_file(outputdir):
        log("Skiping %s : reduced File %s already exist"%(product, "%s/%s_0000.fits"%(outputdir,product)), 1, NOTICE)
        return




    sof_file_name = "%s/%s.sof"%(".", product)
    log("Writing sof file '%s'"%sof_file_name, 1, NOTICE)
    write_sof(sof, sof_file_name)

    ftype = sof.get_type()
    if (not hasattr(sof, "esorex_options"))\
         or (sof.esorex_options.get("recipy", None) is None):
        log("Cannot find esorex recipy for ftype '%s' of SOF '%s'"%(ftype, product), 1, ERROR)
        return


    options = sof.esorex_options.copy()
    options["logdir"] = outputdir
    options["logfile"] = "{product}_log.txt".format(product=product)
    options["outputdir"] = outputdir
    options["outputprefix"] = product
    options["sof"] = sof_file_name


    cmd = esorex_cmd(**options)

    log("Executing "+cmd, 1, NOTICE)
    if os.path.exists("./debug"):
        if hasattr(sof, "_esorex_simulation"):
            return sof._esorex_simulation(outputdir=outputdir)
        log("DEBUG: no simulation found for %s"%(sof.get_type()), 1, NOTICE)
        return
    else:
        erc = os.system(cmd)
        if erc:
            log("esorex command returned error code %d"%erc)


    ### simulation
    # make a false product

def esorex_cmd(**options):
    return """esorex --check-sof-exist={checksofexist} --log-level={loglevel} \
--time={time} --log-dir={logdir} --log-file={logfile} \
--output-dir={outputdir} --suppress-prefix={suppressprefix} \
--output-prefix={outputprefix} {recipy} {sof}""".format(**options)

def print_bool(b):
    if b is "FALSE":
        return "FALSE"
    return "TRUE" if bool(b) else "FALSE"

_esorex_option_formater = {
    "checksofexist":("check-sof-exist",  print_bool),
    "time":     ("time",  print_bool),
    "loglevel": ("log-level",  str),
    "logdir":   ("log-dir", str),
    "logfile":  ("log-file", str),
    "outputdir":("output-dir", str),
    "suppressprefix":("suppress-prefix",print_bool),
    "outputprefix":  ("output-prefix", str),
    "recipy": ("recipy", str),
    "sof":    ("sof",str)
   }












