from .fits  import (GravHDUList, GravFitsList, GravSOF,
                    GravSOFList, record_class)
from .getmethod import build_getmethod, getarray, getmethod
from log import ERROR, WARNING, NOTICE
from .errors import MissingData, WrongData
from config import DARK, FLAT, WAVE, P2VM, esorex_options

class Dark(GravHDUList):
    def _check_data(self):
        ### check here if it is a dark
        pass

class Darks(GravFitsList):
    """ A flat list of darks """
    pass


class DarkSOF(GravSOF):
    """ containes a list of SOF dark file """
    ##
    # All the method are called to the first item
    sof_type = DARK
    esorex_options = esorex_options[DARK]

    def checkout(self):
        if not len(self):
            raise MissingData("SOF is empty expecting at least one dark")

        for obj in self:
            if obj.get_type() != DARK:
                raise WrongData("Expecting only Darks in DARK SOF, got a '%s'"%obj.get_type())

    def assoc_calib(self, alldata):
        ##
        # dark does not need more calib than itself
        return DarkSOF(self)




    ## for debug purpose
    def _esorex_simulation(self, outputdir="."):
        dark = self[0]
        new = GravHDUList(dark)
        prod = self.get_product_name()
        self.log("Building reduced file '%s'"%prod, 1, NOTICE)
        new.writeto("%s/%s_0000.fits"%(outputdir,prod), clobber=True)
        return prod


class DarkSOFList(GravSOFList):
    """ containes a list of SOF dark file """
    subclass = DarkSOF

## mendatory :  need to record all the classes
record_class(DARK, Dark,Darks,DarkSOF,DarkSOFList)
