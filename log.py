import time


ERROR   = 2
WARNING = 4
NOTICE  = 8

try:

    import colorama as col
except:
    str_mtype = {ERROR:"ERROR", WARNING:"WARNING", NOTICE:"NOTICE"}
else:
    str_mtype = {

                 ERROR:col.Fore.RED+"ERROR"+col.Fore.RESET,
                 WARNING:col.Fore.MAGENTA+"WARNING"+col.Fore.RESET,
                 NOTICE:col.Fore.BLUE+"NOTICE"+col.Fore.RESET
                 }

class Log(object):
    """A Log set of function for Gravity """
    _message_format = """[GRAV: {mtype}] {dtype} {date}: {msg}"""
    def log(self, msg, level=1, mtype=WARNING):
        if getattr(self,"verbose", 1)<=level:
            print self._message_format.format(mtype=str_mtype[mtype],
                                        dtype=getattr(self,"dtype",""),
                                        date=time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()),
                                        msg =msg
                                       )


