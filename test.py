
import gravi_reduce as g

try:
    raw
except:
    raw = g.dispatch(g.local.ls())
else:
    g.dispatch(g.local.ls(), raw)

##
# build the soflist only for raw
sof_raw = raw.build_sof()
sof_dark = sof_raw.dark.assoc_calib(raw)
sof_dark.run()

reduced = g.dispatch(g.local.ls(subdir="./reduced"))

sof_flat = sof_raw.flat.assoc_calib(reduced)
sof_flat.run()


sof_p2vm = sof_raw.p2vm.assoc_calib(raw)
sof_p2vm[0].run()
