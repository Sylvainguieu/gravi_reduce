

FLAT, DARK, P2VM, WAVE = "FLAT", "DARK", "P2VM", "WAVE"

##
# If True the header of file is copied and
# files are closed to avoid to have tomany file pointer at
# the same time.
# if False each fits file opened will stay open, which can
# be problematic as each HDU is a file pointer
copy_file = True


## all value must be string they are the default esorex option
## if one add more option,  one need to modify the function esorex_cmd
## located in esorex.py
esorex_default_options = {
    "checksofexist":"TRUE",
    "time": "TRUE",
    "loglevel": "debug",
    "logdir": ".", #must be filed after
    "logfile": "log",
    "outputdir": ".",
    "suppressprefix":"FALSE",
    "outputprefix": "reduced",  #must be filed after
    "recipy":  None,
    "sof":  "tmp.sof"  #must be filed after
}
edo = esorex_default_options

## Each types can have its esorex option
## The obvious change is the recipy name
esorex_options = {DARK: dict(edo,
                           recipy="gravi_all_dark"
                         ),
                  FLAT:
                       dict(edo,
                            recipy="gravi_all_flat"
                           ),
                  WAVE: {}, # no recipy for wave
                  P2VM: dict(edo,
                             recipy="gravi_all_p2vm"
                            )
                   }
