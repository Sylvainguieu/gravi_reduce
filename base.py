from .flat import Flat, Flats
from .dark import Dark, Darks
from .wave import Wave, Waves
from .p2vm import P2vm, P2vms


from .assoc import get_type
from .fits import grav_open, AllTypeDict
from log import Log, WARNING, ERROR, NOTICE
import os
log = Log().log
from config import FLAT, DARK, P2VM, WAVE

###
# Classes for HDUList=individual file for each type
file_class = {FLAT:Flat, DARK:Dark, WAVE:Wave, P2VM:P2vm}
###
# Classes for list of HDUList for each type
list_class = {FLAT:Flats, DARK:Darks, WAVE:Waves, P2VM:P2vms}

def dispatch(list_of_file, output=None):
    """ Take a list of file or gravity fits file (opened with grav_open)
    and dispatch them into a dictionary, function to their type.
    """

    ###
    # create a new output or if output is given
    # populate with the missing type list
    if output is None:
        output = AllTypeDict({})
    else:
        if not isinstance(output, dict):
            raise ValueError("Expecting a dict object for output got %s"%output.__class__)

    already_opened = []
    for typename,cl in list_class.iteritems():
        if not typename in output:
            output[typename] = cl([])
        else:
            already_opened.extend(output[typename].get_file_path())


    for fh in list_of_file:
        opened = False
        for opened_file in already_opened:
            if os.path.samefile(opened_file, fh):
                opened = True
                continue
        if opened:
            continue

        if isinstance(fh, basestring):
            fh = grav_open(fh)

        log("opening file '%s'"%fh, 1, NOTICE)
        ftype = fh.get_type()
        if not ftype in output:
            log("the file '%s' as an unknown type '%s' and will be ignored"%(fh.get_file_name(),ftype),
                1,WARNING)
            continue

        output[ftype].append( file_class[ftype](fh) )
    return output
