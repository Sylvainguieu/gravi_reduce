from .fits  import (GravHDUList, GravFitsList, GravSOF,
                    GravSOFList, record_class)
from .getmethod import build_getmethod, getarray, getmethod
from .assoc import assoc
from errors import MissingData, WrongData
import numpy as np
from log import ERROR, WARNING, NOTICE
from .config import FLAT, DARK, P2VM, WAVE, esorex_options
from .esorex import run


class Wave(GravHDUList):
    def _check_data(self):
        ### check here if it is a flat
        pass

class Waves(GravFitsList):
    """ A list of wavess """
    pass

class WaveSOF(GravSOF):
    """ containes a list of SOF dark file """
    ##
    # All the method are called to the first item
    sof_type = WAVE
    esorex_options = esorex_options[WAVE]

    def checkout(self):
        """
        Wave has no recipies SOF are never ready
        """
        raise WrongData("Wave file has no recipies")

    def assoc_calib(self, alldata):
        return WaveSOF(self)


    def _esorex_simulation(self, outputdir="."):
        return self.get_product_name()


class WaveSOFList(GravSOFList):
    """ containes a list of SOF dark file """
    subclass = WaveSOF

record_class(WAVE, Wave,Waves,WaveSOF,WaveSOFList)
