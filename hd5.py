import numpy as np
import tables

## string size in hd5 table
_header_string_size = 80
FILENAME = "file_name"


def keyfilter(k):
    return k in ["ESO DPR TYPE", "ESO TPL START",
        "ESO DET DIT", "ESO DET NDIT", "DATE-OBS"
        ## more more
        ]


def dtype2Col(dtype):
    """ convert a type to the right table Col subclass """
    dtype = np.dtype(dtype)
    #if dtype is np.dtype(str) or dtype is np.dtype(unicode):
    #    return tables.StringCol

    str_col = dtype.name.capitalize()+"Col"
    return tables.__dict__[str_col]

def make_header_description(header, keyfilter=keyfilter,
                            filename=None):
    """ from a header object with the iteritems method,
    create a description dictionary for Table
    """
    description = {}
    index = 0
    for key, value in header.iteritems():
        if not key: continue
        if not keyfilter(key): continue

        if isinstance(value, tuple):
            value = value[0]  # case of a dictionary where the second tuple
                              # element is a comment
        coltype = dtype2Col(type(value))
        if issubclass(coltype, tables.StringCol):
            col = coltype(_header_string_size, pos=index)
        else:
            col = coltype(pos=index)
        description[key] = col
        index += 1
    if filename is not None:
        description[FILENAME] = tables.StringCol(max(100,len(filename)))
    return description


def populate_header_row(header, row, filename=None):
    """ take a table row and a header (with iteritems method)
    and populate the row with the header.
    the row should have the right description
    """
    if not isinstance( row, tables.tableextension.Row):
        raise TypeError("expecting a table row object as 1st argument got %s"%row)
    for key, value in header.iteritems():
        if isinstance(value, tuple):
            value = value[0]
        try:
            row[key] = value
        except KeyError:
            say("The key '%s' is not defined in table and will be ignored"%key)
            continue
    if filename is not None:
        row[FILENAME] = filename

    row.append()


def make_header_table(header, group, name="table",
                      description="", filename=None):
    return tables.Table(group, name, make_header_description(header, filename=filename))



def record_group(lst, h5file, group="/", name="headers",
                 title="", **kwargs):
    if hot hasattr(h5file.root, name)
        gr = h5file.create_group(group, name, title=title)
    gr = getattr(h5file.root, name)

    add_table(lst, gr, **kwargs)


def write_hd5(lst, file_path, title="", group_name="detector",
              group_title="",
              **kwargs):
    with tables.open_file(file_path, mode = "a",
                           title = title) as fl:
        record_group(lst, fl, name=group_name,
                     title=group_title, **kwargs)






