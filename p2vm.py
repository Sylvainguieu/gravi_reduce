from .fits  import (GravHDUList, GravFitsList, GravSOF,
                    GravSOFList, record_class)

from .getmethod import build_getmethod, getarray, getmethod
from .assoc import assoc
from errors import MissingData
import numpy as np
from log import ERROR, WARNING, NOTICE
from .config import FLAT, DARK, P2VM, WAVE, esorex_options
from .esorex import run


class P2vm(GravHDUList):
    def _check_data(self):
        ### check here if it is a flat
        pass

class P2vms(GravFitsList):
    """ A list of p2vm """
    pass


class P2vmSOF(GravSOF):
    """ containes a list of p2vm SOF p2vm """
    ##
    # All the method are called to the first item
    sof_type = P2VM
    esorex_options = esorex_options[P2VM]


    def checkout(self):
        """
        Return the file name of the First P2vm found.
        """
        ndark = 0
        nflat = 0
        shutters = 0
        np2vm = 0
        nwave = 0
        for obj in self:
            otype = obj.get_type()

            if otype == P2VM:
                np2vm += 1
            if otype == FLAT:
                nflat += 1
                shutters += obj.get_shutters()
            if otype == DARK:
                ndark += 1
            if otype == WAVE:
                nwave +=1

        if not ndark:
            raise MissingData("Missing a dark file for P2VM SOF expecting at least 1 got 0")

        if nflat!=4:
            raise MissingData("Missing or to much flat file for P2VM SOF expecting 4 got %d"%nflat)

        if shutters!=(2+4+8+16):
            raise WrondData("The shutter sequence of Flat is not corect")

        if np2vm<6:
            raise MissingData("Expecting 6 p2vm files for P2VM SOF, got %d"%np2vm)

        if not nwave:
            raise MissingData("Missing a wavefile for P2VM SOF expecting at least 1 got 0")

    def assoc_all_dark(self, lst):
        return self.assoc_by_type(self.get_type(), DARK,
                                  lst)
    def assoc_all_flat(self, lst):
        return self.assoc_by_type(self.get_type(), FLAT,
                                  lst)
    def assoc_all_wave(self, lst):
        return self.assoc_by_type(self.get_type(), WAVE,
                                  lst)

    @getmethod
    def assoc_calib(self, alldata):
        darks = self.assoc_all_dark(alldata)

        flats = self.assoc_all_flat(alldata)

        waves = self.assoc_all_wave(alldata)

        self.log("P2VM File Association found :\n\t{ndarks} darks\n\t\t{darks}\n\t{nflats} Flat\n\t\t{flats}\n\t{nwaves}wave\n\t\t{waves}".format(
            ndarks=len(darks),
            darks="\n\t\t".join([dark.get_file_name() for dark in darks]),
            nflats=len(flats),
            flats="\n\t\t".join([flat.get_file_name() for flat in flats]),
            nwaves=len(waves),
            waves="\n\t\t".join([wave.get_file_name() for wave in waves])
        ), 1, NOTICE)
        ##
        # do not forgot to add self wich is the base sof
        return P2vmSOF(darks+flats+waves+self)



class P2vmSOFList(GravSOFList):
    """ containes a list of SOF dark file """
    subclass = P2vmSOF

record_class(P2VM, P2vm,P2vms,P2vmSOF,P2vmSOFList)
