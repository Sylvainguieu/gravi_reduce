from config import FLAT, DARK, P2VM, WAVE

""" function that get the configuration for each type of file for
file association. e.g. dark for flat etc ....
"""

def dark_flat(obj):
    """ Return the configuration tuple to associate dark with flat """
    return tuple(obj.get_keyword(k) for k in ["ESO DET2 SEQ1 DIT", "ESO DET3 SEQ1 DIT",
                 "ESO INS SPEC RES", "ESO INS POLA MODE"])

def flat_flat(obj):
    """ return the configuration tuple to assoc flats together """
    return tuple(obj.get_keyword(k) for k in ["ESO TPL START"])

def dark_dark(obj):
    """ Darks are one2one product, one dark create one reduced  dark
    the configuration tuple return only the DATE-OBS so the dark
    will match only to itself. Hopefull
    """
    return tuple(obj.get_keyword(k) for k in ["DATE-OBS"])

def wave_wave(obj):
    """ Wave has no recipy, just match the object by itself
    """
    return tuple(obj.get_keyword(k) for k in ["DATE-OBS"])


def p2vm_p2vm(obj):
    """ For p2vm's match the TPL START also the files should be raw """
    return tuple(obj.get_keyword(k) for k in ["ESO TPL START"])+(obj.is_raw(),)
dark_p2vm = p2vm_p2vm
flat_p2vm = p2vm_p2vm
wave_p2vm = p2vm_p2vm



"""
assoc is a dictionary containing the configuratio function
for each combination combination.
For instance when a Flat look for a dark in a list of dark it will
try to match the configuration tuple return by assoc["FLAT"]["DARK"] function
to both the flat and each dark
"""
assoc = {
    DARK:{FLAT: dark_flat,
          DARK:dark_dark
         },
    WAVE:{WAVE: wave_wave},
    FLAT:{DARK: dark_flat,
          FLAT: flat_flat
          },
    P2VM:{
        P2VM:p2vm_p2vm,
        FLAT:flat_p2vm,
        DARK:dark_p2vm,
        WAVE:wave_p2vm
    }
}


"""
This dictionary define the time *IN HOUR* validity of each product
(raw or reduced)
"""
validity = {
        "DARK":48 # 48h time validity
}

"""
the following function return the type of file
"""
def get_type(obj):
    return obj.get_keyword("ESO DPR TYPE")
