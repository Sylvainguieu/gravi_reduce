from .gets import Get, build_getmethod
import numpy as np

class Get(Get):
    def keyword(self, keyname, **kwargs):
        def fget(obj, default=None):
            return obj.get_keyword(keyname)
        fget.__doc__ = """Return the header key '%s'"""%keyname
        kwargs["fget"] = fget
        return self(**kwargs)


getmethod = Get()
getlist  = getmethod
getarray = getmethod(wrapper=np.array, vtype="numerical")


